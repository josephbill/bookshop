CREATE DATABASE bookshop;
\c bookshop;
CREATE TABLE users (id SERIAL PRIMARY KEY, name VARCHAR, role VARCHAR, phone VARCHAR, password VARCHAR);
CREATE TABLE books (id SERIAL PRIMARY KEY, name VARCHAR, price VARCHAR, coverimage VARCHAR, publishingyear VARCHAR);
CREATE TABLE books_users (id SERIAL PRIMARY KEY, userId INTEGER, booksId INTEGER);
ALTER TABLE users ADD COLUMN scanned_in timestamp with time zone;
ALTER TABLE books ADD COLUMN scanned_in timestamp with time zone;
ALTER TABLE books_users ADD COLUMN scanned_in timestamp with time zone;
CREATE DATABASE bookshop_test WITH TEMPLATE bookshop;