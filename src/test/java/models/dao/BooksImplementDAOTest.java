package models.dao;

import models.pojos.Books;
import models.pojos.Users;
import org.junit.jupiter.api.Test;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.awt.print.Book;

public class BooksImplementDAOTest {
//    private static BooksImplementDAO booksImplementDAO;    //these variables are now static.
//    private static UserImplementDAO userImplementDAO;            //these variables are now static.
//    private static Connection conn;                 //these variables are now static.
//
//    @BeforeClass      //changed to @BeforeClass (run once before running any tests in this file)
//    public static void setUp() throws Exception {   //changed to static
//        String connectionString = "jdbc:postgresql://localhost:5432/bookshop_test"; // connect to postgres test database
//        Sql2o sql2o = new Sql2o(connectionString, "postgres", "");          // changed user and pass to null
//        booksImplementDAO = new BooksImplementDAO(sql2o);
//        userImplementDAO = new UserImplementDAO(sql2o);
//        conn = sql2o.open();                        // open connection once before this test file is run
//    }
//
//    @After                                          // run after every test
//    public void tearDown() throws Exception {
//        System.out.println("clearing database");
//        booksImplementDAO.clearAll();           // clear all books after every test
//        userImplementDAO.clearAllUsers();                    // clear all users after every test
//    }
//
//    @AfterClass                                     //run once after all tests in this file completed
//    public static void shutDown() throws Exception{
//        conn.close();                               // close connection once after this entire test file is finished
//        System.out.println("connection closed");
//    }
//
//    @Test
//    public void addingBookSetsId() throws Exception {
//        Books books = new Books("Harry","200","screen.jpg","3003");
//        int originalBookId = books.getId();
//        booksImplementDAO.add(books);
//        assertNotEquals(originalBookId, books.getId());
//    }
//
//    @Test
//    public void existingBooksCanBeFoundById() throws Exception {
//        Books books = new Books("Harry","200","screen.jpg","3003");
//        booksImplementDAO.add(books);
//        Books foundbook = booksImplementDAO.findById(books.getId());
//        assertEquals(books, foundbook);
//    }
//
//    @Test
//    public void addedBooksAreReturnedFromGetAll() throws Exception {
//        Books books = new Books("Harry","200","screen.jpg","3003");
//        booksImplementDAO.add(books);
//        assertEquals(1, booksImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void noBooksReturnsEmptyList() throws Exception {
//        assertEquals(0, booksImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void updateChangesBookContent() throws Exception {
//        Books intialbook = new Books ("Harry Potter","300","screen.png","2003");
//        booksImplementDAO.add(intialbook);
//        booksImplementDAO.update(intialbook.getId(),"Harry","200","screen.png","2003");
//        Books updatebook = booksImplementDAO.findById(intialbook.getId());
//        assertNotEquals(intialbook, updatebook.getName());
//    }
//
//    @Test
//    public void deleteByIdDeletesCorrectBook() throws Exception {
//        Books books = new Books("Harry","200","screen.jpg","3003");
//        booksImplementDAO.add(books);
//        booksImplementDAO.deleteById(books.getId());
//        assertEquals(0, booksImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void clearAllClearsAllBooks() throws Exception {
//        Books books = new Books("Harry","200","screen.jpg","3003");
//        Books otherbook = new Books("harry","200","scrren.png","3002");
//        booksImplementDAO.add(books);
//        booksImplementDAO.add(otherbook);
//        int daoSize = booksImplementDAO.getAll().size();
//        booksImplementDAO.clearAll();
//        assertTrue(daoSize > 0 && daoSize > booksImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void getAllUsersBybookborrowedCorrectly() throws Exception {
//        Books books = new Books("Harry","200","screen.jpg","3003");
//        booksImplementDAO.add(books);
//        int bookId = books.getId();
//        Users newUser = new Users("Joseph", "user","074837483","dskdhjdskd");
//        userImplementDAO.add(newUser);
//        assertEquals(2, booksImplementDAO.seeUserWithParticularBook(bookId).size());
//        assertTrue(booksImplementDAO.seeUserWithParticularBook(bookId).contains(newUser));//things are accurate!
//    }
//
//    @org.junit.Test
//    public void giveBooktoUser() throws Exception {
//
//        Users newUser = new Users("Joseph", "user","074837483","dskdhjdskd");
//        Users otherUser = new Users("Bill", "user","074837483","dskdhjdskd");
//
//        userImplementDAO.add(newUser);
//        userImplementDAO.add(otherUser);
//
//        Books books = new Books("Harry","200","screen.jpg","3003");
//
//        booksImplementDAO.add(books);
//
//        booksImplementDAO.giveBooktoUser(books, newUser);
//        booksImplementDAO.giveBooktoUser(books, otherUser);
//
//        assertEquals(2, booksImplementDAO.seeUserWithParticularBook(books.getId()).size());
//    }



}