package models.dao;

import models.pojos.Books;
import models.pojos.Users;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import static org.junit.jupiter.api.Assertions.*;

public class UserImplementDAOTest {
//    private static BooksImplementDAO booksImplementDAO;    //these variables are now static.
//    private static UserImplementDAO userImplementDAO;            //these variables are now static.
//    private static Connection conn;                 //these variables are now static.
//
//    @BeforeClass      //changed to @BeforeClass (run once before running any tests in this file)
//    public static void setUp() throws Exception {   //changed to static
//        String connectionString = "jdbc:postgresql://localhost:5432/bookshop_test"; // connect to postgres test database
//        Sql2o sql2o = new Sql2o(connectionString, "postgres", "");          // changed user and pass to null
//        booksImplementDAO = new BooksImplementDAO(sql2o);
//        userImplementDAO = new UserImplementDAO(sql2o);
//        conn = sql2o.open();                        // open connection once before this test file is run
//    }
//
//    @After                                          // run after every test
//    public void tearDown() throws Exception {
//        System.out.println("clearing database");
//        booksImplementDAO.clearAll();           // clear all books after every test
//        userImplementDAO.clearAllUsers();                    // clear all users after every test
//    }
//
//    @AfterClass                                     //run once after all tests in this file completed
//    public static void shutDown() throws Exception{
//        conn.close();                               // close connection once after this entire test file is finished
//        System.out.println("connection closed");
//    }
//
//    @Test
//    public void addingUserSetsId() throws Exception {
//        Users users = new Users("Harry","200","073472438938","3003");
//        int originalUserId = users.getId();
//        userImplementDAO.add(users);
//        assertNotEquals(originalUserId, users.getId());
//    }
//
//    @Test
//    public void existingUserCanBeFoundById() throws Exception {
//        Users users = new Users("Harry","admin","073472438938","3003");
//        userImplementDAO.add(users);
//        Users founduser = userImplementDAO.findById(users.getId());
//        assertEquals(users, founduser);
//    }
//
//    @Test
//    public void addedUsersAreReturnedFromGetAll() throws Exception {
//        Users users = new Users("Harry","admin","073472438938","3003");
//        userImplementDAO.add(users);
//        assertEquals(1, userImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void noUsersReturnsEmptyList() throws Exception {
//        assertEquals(0, userImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void updateChangesUsersContent() throws Exception {
//        Users intialusers = new Users("Harry","admin","073472438938","3003");
//        userImplementDAO.add(intialusers);
//        userImplementDAO.update(intialusers.getId(),"Harry","admin","089893844934","2003");
//        Users updateUsers = userImplementDAO.findById(intialusers.getId());
//        assertNotEquals(intialusers, updateUsers.getName());
//    }
//
//    @Test
//    public void deleteByIdDeletesCorrectUser() throws Exception {
//        Users users = new Users("Harry","admin","073472438938","3003");
//        userImplementDAO.add(users);
//        userImplementDAO.deleteById(users.getId());
//        assertEquals(0, userImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void clearAllClearsAllUsers() throws Exception {
//        Users users = new Users("Harry","admin","073472438938","3003");
//        Users otheruser = new Users("Joseph","admin","073472438938","3003");
//        userImplementDAO.add(users);
//        userImplementDAO.add(otheruser);
//        int daoSize = userImplementDAO.getAll().size();
//        userImplementDAO.clearAllUsers();
//        assertTrue(daoSize > 0 && daoSize > userImplementDAO.getAll().size());
//    }
//
//    @Test
//    public void getAllUsersBybookborrowedCorrectly() throws Exception {
//        Books books = new Books("Harry","200","screen.jpg","3003");
//        booksImplementDAO.add(books);
//        int bookId = books.getId();
//        Users newUser = new Users("Joseph", "user","074837483","dskdhjdskd");
//        userImplementDAO.add(newUser);
//        assertEquals(2, booksImplementDAO.seeUserWithParticularBook(bookId).size());
//        assertTrue(booksImplementDAO.seeUserWithParticularBook(bookId).contains(newUser));//things are accurate!
//    }

}