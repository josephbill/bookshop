import com.google.gson.Gson;
import exceptions.ApiException;
import models.dao.BooksImplementDAO;
import models.dao.UserImplementDAO;
import models.pojos.Books;
import models.pojos.Users;
import org.sql2o.Sql2o;
import java.util.*;

import static spark.Spark.*;


public class Api {
    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }
    public static void main(String[] args) throws Exception {
        port(getHerokuAssignedPort());
        staticFileLocation("/public");
        //connection to postgres
        String connectionString = "jdbc:postgresql://localhost:5432/bookshop";      //connect to bookshop, not bookshop_test!
        Sql2o sql2o = new Sql2o(connectionString, "postgres", ""); //use your own credentials

        //connection to postgres on heroku
//        String connectionString = "jdbc:postgresql://ec2-3-216-221-31.compute-1.amazonaws.com:5432/d40cinp2h997k"; //!
//        Sql2o sql2o = new Sql2o(connectionString, "rrcterucpmzjza", "5fa215bee8f924b43601ec5daf5b7fb456a73fb400f627fb9335097c834450db"); //!

        //reference to my dao implementing classes
        UserImplementDAO userImplementDAO = new UserImplementDAO(sql2o);
        BooksImplementDAO booksImplementDAO = new BooksImplementDAO(sql2o);
        //Gson object
        //A Java serialization/deserialization library to convert Java Objects into JSON and back
        Gson gson = new Gson();
        //spark routes

        //API's
        //CREATE
        //post api for giving book to user
        post("/users/:userId/books/:bookId", "application/json", (req, res) -> {

            int userId = Integer.parseInt(req.params("userId"));  //getting id for user from input /filter
            int bookId = Integer.parseInt(req.params("bookId")); //getting id for book from input /filter
            Users users = userImplementDAO.findById(userId); //fetching id for User from its POJO
            Books books = booksImplementDAO.findById(bookId); //fetching id for Books from its POJO
            // POJOs are used for increasing the readability and re-usability of a program.


            if (users != null && books != null){ //condition checking for user and book id exists for the record
                //both exist and can be associated
                booksImplementDAO.giveBooktoUser(books, users); //attaching book to user from method in POJO
                res.status(201);
                //return success data as JSON
                return gson.toJson(String.format("Book '%s' has been rented to '%s'",books.getName(), users.getName()));
            }
            else {
//                throw new ApiException(404, String.format("Book or User does not exist"));
                return gson.toJson(String.format("Book or User does not exist"));
            }
        });

        //getting book given to a user
        get("/users/:id/books", "application/json", (req, res) -> {
            int userId = Integer.parseInt(req.params("id")); //getting id filter
            Users usertofind = userImplementDAO.findById(userId); //finding user by  id
            if (usertofind == null){ //if null
               // throw new ApiException(404, String.format("No User with the id: \"%s\" exists", req.params("id")));
                return gson.toJson(String.format("This user does not exist"));

            }
            else if (userImplementDAO.getAllBooksRentedtoSpecificUser(userId).size()==0){ //if user is there but no book listed
                return "{\"message\":\"No book has been rented out to this user.\"}";
            }
            else { //else return JSON of the books rented to user
                return gson.toJson(userImplementDAO.getAllBooksRentedtoSpecificUser(userId));
            }
        });


        //FILTERS
        //A filter is an object that is invoked at the preprocessing and postprocessing of a request.
        //filter for exception
//        exception(ApiException.class, (exception, req, res) -> {
//            ApiException err = exception;//when exception happens generate an exception object
//            Map<String, Object> jsonMap = new HashMap<>(); //new hashmap that will store info about the error
//            jsonMap.put("status", err.getStatusCode()); //putting the status code for the error in hashmap
//            jsonMap.put("errorMessage", err.getMessage()); //getting the msg translation for our error code
//            res.type("application/json"); //content type for response
//            res.status(err.getStatusCode()); //give status code in JSON
//            res.body(gson.toJson(jsonMap)); //give body of error in JSON also , the status code and error message
//        });

        //filter for expected content type for data returned
        after((req, res) ->{
            res.type("application/json");
        });

    }

}
