import com.google.gson.Gson;
import exceptions.ApiException;
import models.dao.BooksImplementDAO;
import models.dao.UserImplementDAO;
import models.pojos.Books;
import models.pojos.Users;
import org.sql2o.Sql2o;
import spark.ModelAndView;
import spark.template.handlebars.HandlebarsTemplateEngine;
import spark.utils.IOUtils;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static spark.Spark.*;


public class App {
    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }
    public static void main(String[] args) throws Exception {
        port(getHerokuAssignedPort());
        staticFileLocation("/public");

        //connection to postgres on local environment
        String connectionString = "jdbc:postgresql://localhost:5432/bookshop";      //connect to bookshop, not bookshop_test!
        Sql2o sql2o = new Sql2o(connectionString, "postgres", ""); //use your own credentials
        //connection to postgres on heroku
//        String connectionString = "jdbc:postgresql://ec2-3-216-221-31.compute-1.amazonaws.com:5432/d40cinp2h997k"; //!
//        Sql2o sql2o = new Sql2o(connectionString, "rrcterucpmzjza", "5fa215bee8f924b43601ec5daf5b7fb456a73fb400f627fb9335097c834450db"); //!
        //reference to my dao implementing classes
        UserImplementDAO userImplementDAO = new UserImplementDAO(sql2o);
        BooksImplementDAO booksImplementDAO = new BooksImplementDAO(sql2o);
        //Gson object
        //A Java serialization/deserialization library to convert Java Objects into JSON and back
        Gson gson = new Gson();
        //spark routes

        //this is how u add routes
        //homepage
        get("/", (request, response) -> { //request for route happens at this location
            Map<String, Object> model = new HashMap<String, Object>(); // new model is made to store information
            return new ModelAndView(model, "auth.hbs"); // assemble individual pieces and render
        }, new HandlebarsTemplateEngine());
        //add book
        get("/addbooks", (request, response) -> { //request for route happens at this location
            Map<String, Object> model = new HashMap<String, Object>(); // new model is made to store information
            List<Books> books = booksImplementDAO.getAll(); //fetching all books
            model.put("books",books); //adding to model //here u can also save details in sessions
            return new ModelAndView(model, "addbooks.hbs"); // assemble individual pieces and render
        }, new HandlebarsTemplateEngine());
        //view books
        //get: show all enteries from images table and also form for adding content
        get("/vbooks", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            //getting all data
            List<Books> mybooks = booksImplementDAO.getAll();
            model.put("mybooks",mybooks);
            return new ModelAndView(model, "viewbooks.hbs");
        }, new HandlebarsTemplateEngine());
        //view books
        get("/vusers", (request, response) -> { //request for route happens at this location
            Map<String, Object> model = new HashMap<String, Object>(); // new model is made to store information
            List<Users> users = userImplementDAO.getAll(); //fetching all books
            model.put("users",users); //adding to model //here u can also save details in sessions
            return new ModelAndView(model, "viewusers.hbs"); // assemble individual pieces and render
        }, new HandlebarsTemplateEngine());

        //form posts
        //post: process a form to create a new account
        post("/authsignup", (req, res) -> { //new
            Map<String, Object> model = new HashMap<>();
            String name = req.queryParams("name");
            String role = req.queryParams("role");
            String phone = req.queryParams("phone");
            //password encryption
            /*; Plain-text password initialization. */
            String password = req.queryParams("password");
            String encryptedpassword = null; //this is what we will send to the database
            try
            {
                /* MessageDigest instance for MD5. */
               // MessageDigest class represents a cryptographic hash function
                // which can calculate a message digest from binary data.
                MessageDigest m = MessageDigest.getInstance("MD5");

                /* Add plain-text password bytes to digest using MD5 update() method. */
                m.update(password.getBytes());

                /* Convert the hash value into bytes */
                byte[] bytes = m.digest();

                /* The bytes array has bytes in decimal form. Converting it into hexadecimal format. */
                StringBuilder s = new StringBuilder();
                for(int i=0; i< bytes.length ;i++)
                {
                    s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }

                /* Complete hashed password in hexadecimal format */
                encryptedpassword = s.toString(); //here is the password we pass to the database
            }
            catch (java.security.NoSuchAlgorithmException e)
            {
                e.printStackTrace();
            }
            //creating new user object
            Users newUser = new Users(name,role,phone,encryptedpassword);
            //method to add new user in class implementing the DAO
            userImplementDAO.add(newUser);
            //refresh page if user is successfully registered
            res.redirect("/");
            return null;
        }, new HandlebarsTemplateEngine());

        //post: process a form to login to account
        post("/authlogin", (req, res) -> { //new
            Map<String, Object> model = new HashMap<>();
            String phone = req.queryParams("phone");
            String password = req.queryParams("password");
            String encryptedpassword = null; //this is what we will send to the database
            try
            {
                /* MessageDigest instance for MD5. */
                // MessageDigest class represents a cryptographic hash function
                // which can calculate a message digest from binary data.
                MessageDigest m = MessageDigest.getInstance("MD5");

                /* Add plain-text password bytes to digest using MD5 update() method. */
                m.update(password.getBytes());

                /* Convert the hash value into bytes */
                byte[] bytes = m.digest();

                /* The bytes array has bytes in decimal form. Converting it into hexadecimal format. */
                StringBuilder s = new StringBuilder();
                for(int i=0; i< bytes.length ;i++)
                {
                    s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }

                /* Complete hashed password in hexadecimal format */
                encryptedpassword = s.toString(); //here is the password we pass to the database
            }
            catch (java.security.NoSuchAlgorithmException e)
            {
                e.printStackTrace();
            }
            //checking if record exists
            userImplementDAO.authenticateUser(phone,encryptedpassword);
            res.redirect("/addbooks");
            return null;
        }, new HandlebarsTemplateEngine());

        //post: process a form to adding a new book
        post("/postbook", (req, res) -> { //new
            Map<String, Object> model = new HashMap<>();
            //OutputStream is an abstract class that represents writing output.
            // There are many different OutputStream classes,
            // and they write out to certain things (like the screen, or Files, or byte arrays, or network connections, or etc).
            // InputStream classes access the same things, but they read data in from them.
            OutputStream outputStream;

            //image
            //here we declare our attribute for multipart form submission , and also the file to write to from our output stream
            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("c:/Users/Billy/IdeaProjects/InsertImage/src/main/resources/public/"));
            //Part interface represents a part or form item that was received within a multipart/form-data POST request.
            //Some important methods are getInputStream() , write(String fileName) that we can use to read and write file.
            //get part fetches file attachments
            //("image") is the name of the file input in HTML
            Part filePart = null;
            try {
                try{
                    filePart = req.raw().getPart("bookimg");

                }catch (javax.servlet.ServletException e){
                    e.printStackTrace();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
            //reading data from inputstream
            try (InputStream inputStream = filePart.getInputStream()) {
                //if file is there , we use the ourtputStream to write file to our storage location/folder
                outputStream = new FileOutputStream("c:/Users/Billy/IdeaProjects/BookShop/src/main/resources/public/" + filePart.getSubmittedFileName());
                //IOUtils provide utility methods for reading, writing and copying files.
                IOUtils.copy(inputStream, outputStream);
                //fetching other input
                String name = req.queryParams("bookname");
                String price = req.queryParams("bookprice");
                String year = req.queryParams("bookyear");
                //creating new object
                Books books = new Books(name,price,String.valueOf(filePart.getSubmittedFileName()),year);
                //adding object to method to insert to db table
                booksImplementDAO.add(books);
                //closing our output stream process
                outputStream.close();
                //redirecting user to new screen after post
                res.redirect("/vbooks");
            } catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }, new HandlebarsTemplateEngine());



        //update book
        //get: show a form to update a book
        get("/books/:id/edit", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("editBook", true); //for conditional //what form to show update or submit form
            Books books = booksImplementDAO.findById(Integer.parseInt(req.params("id")));
            model.put("book", books);
            //model.put("mybooks", booksImplementDAO.getAll()); //refresh list of links for navbar
            return new ModelAndView(model, "addbooks.hbs");
        }, new HandlebarsTemplateEngine());

        //post: process a form to update a book
        post("/books/:id", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            OutputStream outputStream;

            //image
            //here we declare our attribute for multipart form submission , and also the file to write to from our output stream
            req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("c:/Users/Billy/IdeaProjects/InsertImage/src/main/resources/public/"));
            //Part interface represents a part or form item that was received within a multipart/form-data POST request.
            //Some important methods are getInputStream() , write(String fileName) that we can use to read and write file.
            //get part fetches file attachments
            //("image") is the name of the file input in HTML
            Part filePart = null;
            try{
                try {
                    filePart = req.raw().getPart("bookimg");
                } catch (javax.servlet.ServletException e){
                    e.printStackTrace();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
            //reading data from inputstream
            try (InputStream inputStream = filePart.getInputStream()) {
                //if file is there , we use the ourtputStream to write file to our storage location/folder
                outputStream = new FileOutputStream("c:/Users/Billy/IdeaProjects/BookShop/src/main/resources/public/" + filePart.getSubmittedFileName());
                //IOUtils provide utility methods for reading, writing and copying files.
                IOUtils.copy(inputStream, outputStream);
                //fetching other input
                int idOfBooktoEdit = Integer.parseInt(req.params("id"));//fetching id from url
                String name = req.queryParams("bookname");//other details from form
                String price = req.queryParams("bookprice");
                String year = req.queryParams("bookyear");
                //creating new object
                Books books = new Books(name, price, String.valueOf(filePart.getSubmittedFileName()), year);
                //adding object to method to insert to db table
                booksImplementDAO.update(idOfBooktoEdit,name,price,String.valueOf(filePart.getSubmittedFileName()),year);
                //closing our output stream process
                outputStream.close();
                //redirecting user to new screen after post
                res.redirect("/vbooks");

            }catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }, new HandlebarsTemplateEngine());

        //delete book by id
        //get: delete an individual task
        get("/books/:id/delete", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            int idOfTaskToDelete = Integer.parseInt(req.params("id")); //fetching id from url
            booksImplementDAO.deleteById(idOfTaskToDelete); //implementing delete method
            res.redirect("/vbooks");
            return null;
        }, new HandlebarsTemplateEngine());



    }
}
