package models.dao;

import models.pojos.Books;
import models.pojos.Users;

import java.util.List;

public interface UserDAO {
    //LIST
    List<Users> getAll(); //get all users

    //CREATE
    void add (Users users); //adding users
    //authenticate
    List<Users> authenticateUser(String phone,String password);

    //READ
    Users findById(int id); //fetch user by id
    List<Books> getAllBooksRentedtoSpecificUser(int userid);

    //UPDATE
    void update(int id, String name,String role,String phone,String password); //update a user

    //DELETE
    void deleteById(int id);  //delete user via id
    void clearAllUsers(); //clear all users
}
