package models.dao;

import models.pojos.Books;
import models.pojos.Users;

import java.util.List;

public interface BooksDAO {
    //create
    void add(Books books); //adding  a new book
    void giveBooktoUser(Books books, Users users); //attaching user to a book when they rent //many to many relationship

    //read
    List<Books> getAll();  //getting a list of all books in db
    List<Users> seeUserWithParticularBook(int bookid); //getting users who have a particular book
    Books findById(int id); //finding our books by id

    //UPDATE
    void update(int id, String name,String price,String coverimage,String publishingimage); //update a book

    //delete
    void deleteById(int id); //delete record based off id //in the master table and also in the join table
    void clearAll(); //clear all records in db for foodtype table
}
