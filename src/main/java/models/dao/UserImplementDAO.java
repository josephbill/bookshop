package models.dao;

import models.pojos.Books;
import models.pojos.Users;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;

public class UserImplementDAO implements UserDAO {
    private final Sql2o sql2o;

    public UserImplementDAO(Sql2o sql2o){
        this.sql2o = sql2o;
    }


    @Override
    public List<Users> getAll() {
        try(Connection con = sql2o.open()){
            return con.createQuery("SELECT * FROM users")
                    .executeAndFetch(Users.class);
        }
    }

    @Override
    public void add(Users users) {
        String sql = "INSERT INTO users(name,role,phone,password) VALUES (:name,:role,:phone,:password)";
        try(Connection con = sql2o.open()){
               int id  = (int) con.createQuery(sql,true)
                       .bind(users)
                       .executeUpdate()
                       .getKey();
                       users.setId(id);
        }catch (Sql2oException sql2oException){
               sql2oException.printStackTrace();
        }
    }

    @Override
    public List<Users> authenticateUser(String phone, String password) {
        try(Connection con = sql2o.open()){
            String sql = "SELECT * FROM users WHERE password= :password";
            return con.createQuery(sql)
                    .addParameter("password",password)
                    .executeAndFetch(Users.class);
        }
    }

    @Override
    public Users findById(int id) {
        try(Connection con = sql2o.open()){
            return con.createQuery("SELECT * FROM users WHERE id = :id")
                    .addParameter("id", id)
                    .executeAndFetchFirst(Users.class);
        }
    }

    @Override
    public List<Books> getAllBooksRentedtoSpecificUser(int userid) {
        List<Books> books = new ArrayList(); //empty list
        String joinQuery = "SELECT booksid FROM books_users WHERE userid = :userId";

        try (Connection con = sql2o.open()) {
            List<Integer> allBookIds = con.createQuery(joinQuery)
                    .addParameter("userId", userid)
                    .executeAndFetch(Integer.class);
            for (Integer bookid : allBookIds){
                String bookQuery = "SELECT * FROM books WHERE id = :booksid";
                books.add(
                        con.createQuery(bookQuery)
                                .addParameter("booksid", bookid)
                                .executeAndFetchFirst(Books.class));
            }
        } catch (Sql2oException ex){
            System.out.println(ex);
        }
        return books;
    }


    @Override
    public void update(int id, String name, String role, String phone, String password) {
        String sql = "UPDATE users SET (name, role, phone, password) = (:name, :role, :phone, :password) WHERE id=:id"; //CHECK!!!
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("name", name)
                    .addParameter("role", role)
                    .addParameter("phone", phone)
                    .addParameter("password", password)
                    .addParameter("id", id)
                    .executeUpdate();
        } catch (Sql2oException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void deleteById(int id) {
        String sql = "DELETE from users WHERE id=:id"; //raw sql
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("id", id)
                    .executeUpdate();
        } catch (Sql2oException ex){
            System.out.println(ex);
        }
    }

    @Override
    public void clearAllUsers() {
        String sql = "DELETE from users"; //raw sql
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .executeUpdate();
        } catch (Sql2oException ex){
            System.out.println(ex);
        }
    }
}
