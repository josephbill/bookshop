package models.dao;

import models.pojos.Books;
import models.pojos.Users;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;

public class BooksImplementDAO implements BooksDAO {
    private final Sql2o sql2o;

    public BooksImplementDAO(Sql2o sql2o){
        this.sql2o = sql2o;
    }
    @Override
    public void add(Books books) {
        String sql = "INSERT INTO books(name,price,coverimage,publishingyear) VALUES (:name,:price,:coverimage,:publishingyear)";
        try(Connection con = sql2o.open()){
            int id  = (int) con.createQuery(sql,true)
                    .bind(books)
                    .executeUpdate()
                    .getKey();
            books.setId(id);
        }catch (Sql2oException sql2oException){
            sql2oException.printStackTrace();
        }
    }

    @Override
    public void giveBooktoUser(Books books, Users users) {
        String sql = "INSERT INTO books_users (userid, booksid) VALUES (:userId, :bookId)";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("userId", users.getId())
                    .addParameter("bookId", books.getId())
                    .executeUpdate();
        } catch (Sql2oException ex){
            System.out.println(ex);
        }
    }

    @Override
    public List<Books> getAll() {
        try(Connection con = sql2o.open()){
            return con.createQuery("SELECT * FROM books")
                    .executeAndFetch(Books.class);
        }
    }

    @Override
    public List<Users> seeUserWithParticularBook(int bookid) {
        List<Users> users = new ArrayList();
        String joinQuery = "SELECT userid FROM books_users WHERE booksid = :booksid";

        try (Connection con = sql2o.open()) {
            List<Integer> allUsersId = con.createQuery(joinQuery)
                    .addParameter("booksid", bookid)
                    .executeAndFetch(Integer.class); //what is happenibookidng in the lines above?
            for (Integer userId : allUsersId){
                String restaurantQuery = "SELECT * FROM users WHERE id = :userId";
                users.add(
                        con.createQuery(restaurantQuery)
                                .addParameter("userid", userId)
                                .executeAndFetchFirst(Users.class));
            } //why are we doing a second sql query - set? to filter our restaurants table and get the restaurants matching our filter id
        } catch (Sql2oException ex){
            System.out.println(ex);
        }
        return users;
    }

    @Override
    public Books findById(int id) {
        try(Connection con = sql2o.open()){
            return con.createQuery("SELECT * FROM books WHERE id = :id")
                    .addParameter("id", id)
                    .executeAndFetchFirst(Books.class);
        }
    }

    @Override
    public void update(int id, String name, String price, String coverimage, String publishingyear) {
        String sql = "UPDATE books SET (name, price, coverimage, publishingyear) = (:name, :price, :coverimage, :publishingyear) WHERE id=:id"; //CHECK!!!
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("name", name)
                    .addParameter("price", price)
                    .addParameter("coverimage", coverimage)
                    .addParameter("publishingyear", publishingyear)
                    .addParameter("id", id)
                    .executeUpdate();
        } catch (Sql2oException ex) {
            System.out.println(ex);
        }
    }

    @Override
    public void deleteById(int id) {
        String sql = "DELETE from books WHERE id=:id"; //raw sql
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("id", id)
                    .executeUpdate();
        } catch (Sql2oException ex){
            System.out.println(ex);
        }
    }

    @Override
    public void clearAll() {
        String sql = "DELETE from books"; //raw sql
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .executeUpdate();
        } catch (Sql2oException ex){
            System.out.println(ex);
        }
    }
}
