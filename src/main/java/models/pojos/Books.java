package models.pojos;

import java.sql.Timestamp;
import java.util.Objects;

public class Books {
    private int id;
    public String name;
    public String price;
    public String coverimage;
    public String publishingyear;
    public Timestamp scanned_in;
    private boolean completed; //checking that object is only used when executed


    public Books(String name, String price, String coverimage, String publishingyear) {
        this.name = name;
        this.price = price;
        this.coverimage = coverimage;
        this.publishingyear = publishingyear;
        this.completed = false;

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCoverimage() {
        return coverimage;
    }

    public void setCoverimage(String coverimage) {
        this.coverimage = coverimage;
    }

    public String getPublishingyear() {
        return publishingyear;
    }

    public void setPublishingyear(String publishingyear) {
        this.publishingyear = publishingyear;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Timestamp getscanned_in() {
        return scanned_in;
    }

    public void setscanned_in(Timestamp scanned_in) {
        this.scanned_in = scanned_in;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Users)) return false;
        Books task = (Books) o;
        return completed == task.completed &&
                id == task.id &&
                Objects.equals(name, task.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, completed, id);
    }
}
