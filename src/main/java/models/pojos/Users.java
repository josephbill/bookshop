package models.pojos;

import java.sql.Timestamp;
import java.util.Objects;

public class Users {
    private int id;
    public String name;
    public String role;
    public String phone;
    public String password;
    private boolean completed; //checking that object is only used when executed
    public Timestamp scanned_in;


    public Users(String name, String role, String phone, String password) {
        this.name = name;
        this.role = role;
        this.phone = phone;
        this.password = password;
        this.completed = false;
    }

    public Timestamp getscanned_in() {
        return scanned_in;
    }

    public void setscanned_in(Timestamp scanned_in) {
        this.scanned_in = scanned_in;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Users)) return false;
        Users task = (Users) o;
        return completed == task.completed &&
                id == task.id &&
                Objects.equals(name, task.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, completed, id);
    }
}
